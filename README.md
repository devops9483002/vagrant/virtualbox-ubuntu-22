# Vagrant VirtualBox Ubuntu 22

## Usage

1. If using WSL2, enable Windows access:

    ```bash
    export VAGRANT_WSL_ENABLE_WINDOWS_ACCESS="1"
    ```
